#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
extern void setkeyboard_provoke();
extern void fillrectangle();
extern void printstring();
extern void rectangle();
extern void printchar();
extern void charmap();
extern void vline();
extern void hline();
extern char* itoa();
extern void tick();
extern void setp();
struct window{
  int wx;
  int wy;
  int wl;
  int wh;
  int wbc;
  int wtc;
  int wfc;
  char* wtext;
};
int x, y;
bool held, menu;
void cursor()
{
  if (!held)
    fillrectangle(x * 8, y * 8, 8, 8, 4);
  else
    rectangle(x * 8, y * 8, 8, 8, 4);
}
void infobar()
{
  fillrectangle(0, 192, 320, 8, 10);
  fillrectangle(0, 192, 24, 8, 12);
  printstring("AOS", 11, 1, 192);
  if (menu)
    fillrectangle(0, 80, 40, 112, 12);
}
void screen()
{
  fillrectangle(0, 0, 320, 192, 1);
}
struct window update[2];
void windows()
{
  for (int a = 0; a < 2; a++)
  {
    struct window A = update[a];
    fillrectangle(A.wx * 8, A.wy * 8, A.wl * 8, A.wh * 8, A.wbc);
    rectangle(A.wx * 8, A.wy * 8, A.wl * 8, A.wh * 8, A.wfc);
    hline(A.wx * 8, ++A.wy * 8, A.wl * 8, A.wfc);
    printstring(A.wtext, A.wtc, (A.wx * 8) + 1, (--A.wy * 8) + 1);
  }
}
void cursor_prov(char c)
{
  if (c == 'U' && y > 0)
    y--;
  else if (c == 'D' && y < 23)
    y++;
  else if (c == 'L' && x > 0)
    x--;
  else if (c == 'R' && x < 39)
    x++;
  if (c == '\n')
   held = !held;
  screen();
  if (y == 23)
  {
    if (x == 0 || x == 1 || x == 2)
    {
      if (c == '\n')
      {
        menu = !menu;
        held = false;
      }
    }
  }
  windows();
  if (menu)
    infobar();
  cursor();
  char* temp = "";
  printstring("x:", 6, 0, 0);
  printstring(itoa(x, temp, 10), 6, 16, 0);
  printstring("y:", 6, 0, 8);
  printstring(itoa(y, temp, 10), 6, 16, 8);
  printstring("held:", 6, 0, 16);
  printstring(itoa(((int)held), temp, 10), 6, 40, 16);
  tick();
}
struct window test;
struct window testa;
void ui_init()
{
  test.wx = 8;
  test.wy = 8;
  test.wl = 8;
  test.wh = 8;
  test.wtc = 2;
  test.wbc = 3;
  test.wfc = 4;
  test.wtext = "Welcome!";
  update[0] = test;
  testa.wx = 20;
  testa.wy = 4;
  testa.wl = 6;
  testa.wh = 6;
  testa.wtc = 4;
  testa.wbc = 5;
  testa.wfc = 6;
  testa.wtext = "Hello";
  update[1] = testa;
  screen();
  infobar();
  windows();
  cursor();
  setkeyboard_provoke(cursor_prov);
  tick();
}
