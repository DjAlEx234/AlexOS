#!/bin/bash
export PREFIX="$HOME/opt/cross"
export TARGET=i686-elf
export PATH="$PREFIX/bin:$PATH"
cd Raw\ Code/
nasm -felf32 boot.asm -o ../Output\ Code/boot.o
i686-elf-gcc -c kernel.c -o ../Output\ Code/kernel.o -std=gnu99 -ffreestanding -O2 -Wall -Wextra
i686-elf-gcc -c terminalio.c -o ../Output\ Code/tio.o -std=gnu99 -ffreestanding -O2 -Wall -Wextra
i686-elf-gcc -c interrupt.c -o ../Output\ Code/int.o -std=gnu99 -ffreestanding -O2 -Wall -Wextra
i686-elf-gcc -c isrs.c -o ../Output\ Code/isrs.o -std=gnu99 -ffreestanding -O2 -Wall -Wextra
i686-elf-gcc -c keyboard.c -o ../Output\ Code/kb.o -std=gnu99 -ffreestanding -O2 -Wall -Wextra
i686-elf-gcc -c bigoneuse.c -o ../Output\ Code/bou.o -std=gnu99 -ffreestanding -O2 -Wall -Wextra
i686-elf-gcc -c debug.c -o ../Output\ Code/dg.o -std=gnu99 -ffreestanding -O2 -Wall -Wextra
i686-elf-gcc -c graphic.c -o ../Output\ Code/vgad.o -std=gnu99 -ffreestanding -O2 -Wall -Wextra
i686-elf-gcc -c font.c -o ../Output\ Code/font.o -std=gnu99 -ffreestanding -O2 -Wall -Wextra
i686-elf-gcc -c ui.c -o ../Output\ Code/ui.o -std=gnu99 -ffreestanding -O2 -Wall -Wextra
cd ../Output\ Code/
i686-elf-gcc -T linker.ld -o ../alexos.bin -ffreestanding -O2 -nostdlib boot.o kernel.o tio.o int.o isrs.o kb.o bou.o dg.o vgad.o font.o ui.o -lgcc
cd ..
cp alexos.bin Archive/Current/alexos.bin
cp alexos.bin GrubBoot/kernel.bin
grub-mkrescue -o alexos.iso GrubBoot
qemu-system-i386 -cdrom alexos.iso -m 512M -serial file:out.log
