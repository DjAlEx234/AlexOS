#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
static inline void outb(uint16_t port, uint8_t val)
{
	asm volatile ( "outb %0, %1" : : "a"(val), "Nd"(port) );
}
extern void terminal_writestring();
struct regs
{
    unsigned int gs, fs, es, ds;
    unsigned int edi, esi, ebp, esp, ebx, edx, ecx, eax;
    unsigned int int_no, err_code;
    unsigned int eip, cs, eflags, useresp, ss;
};
char* itoa(int value, char* result, int base) {
		if (base < 2 || base > 36) { *result = '\0'; return result; }
		char* ptr = result, *ptr1 = result, tmp_char;
		int tmp_value;
		do {
			tmp_value = value;
			value /= base;
			*ptr++ = "zyxwvutsrqponmlkjihgfedcba9876543210123456789abcdefghijklmnopqrstuvwxyz" [35 + (tmp_value - value * base)];
		} while ( value );
		*ptr-- = '\0';
		while(ptr1 < ptr) {
			tmp_char = *ptr;
			*ptr--= *ptr1;
			*ptr1++ = tmp_char;
		}
		return result;
	}
void error(struct regs *r)
{
	char* new = "";
	terminal_writestring("\nEAX: 0x");
	terminal_writestring(itoa(r->eax, new, 16));
	new = "";
	terminal_writestring("\nECX: 0x");
	terminal_writestring(itoa(r->ecx,  new, 16));
	new = "";
	terminal_writestring("\nEDI: 0x");
	terminal_writestring(itoa(r->edi,  new, 16));
	new = "";
	terminal_writestring("\nESI: 0x");
	terminal_writestring(itoa(r->esi,  new, 16));
	new = "";
	terminal_writestring("\nEBP: 0x");
	terminal_writestring(itoa(r->ebp,  new, 16));
	new = "";
	terminal_writestring("\nESP: 0x");
	terminal_writestring(itoa(r->esp,  new, 16));
	new = "";
	terminal_writestring("\nEBX: 0x");
	terminal_writestring(itoa(r->ebx,  new, 16));
	new = "";
	terminal_writestring("\nEDX: 0x");
	terminal_writestring(itoa(r->edx,  new, 16));
	new = "";
	terminal_writestring("\nINT: 0x");
	terminal_writestring(itoa(r->int_no,  new, 16));
	new = "";
	terminal_writestring("\nERR: 0x");
	terminal_writestring(itoa(r->err_code,  new, 16));
	new = "";
	terminal_writestring("\nES: 0x");
	terminal_writestring(itoa(r->es,  new, 16));
	new = "";
	terminal_writestring("\nGS: 0x");
	terminal_writestring(itoa(r->gs,  new, 16));
	new = "";
	terminal_writestring("\nDS: 0x");
	terminal_writestring(itoa(r->ds,  new, 16));
	new = "";
	terminal_writestring("\nFS: 0x");
	terminal_writestring(itoa(r->fs,  new, 16));
	new = "";
	while (1)
			asm("hlt");
}
int pit_ticks = 0;
void pit_phase(int hz)
{
    int divisor = 1193180 / hz;
    outb(0x43, 0x36);
    outb(0x40, divisor & 0xFF);
    outb(0x40, (divisor >> 8) & 0xFF);
}
void pit_handler(struct regs *r)
{
	pit_ticks++;
}
void sleep(int seconds, void (*test)(void))
{
    while (pit_ticks % (seconds * 100) != 0)terminal_writestring("");
    test();
}
