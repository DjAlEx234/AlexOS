#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
extern void terminal_initialize();
extern void terminal_writestring();
extern void terminal_setcolor();
extern void initallstuff();
extern void vgasetup();
extern void sleep();
void kernel_main(void)
{
	terminal_initialize();
	initallstuff();
	terminal_setcolor(9, 0);
	terminal_writestring("Welcome to AlexOS!\nNow booting to VGA 320x200x256...");
	sleep(2, vgasetup);
	while (true) {}
}
