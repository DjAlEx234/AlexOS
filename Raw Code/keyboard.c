#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
static inline uint8_t inb(uint16_t port)
{
    uint8_t ret;
    asm volatile ( "inb %1, %0"
                   : "=a"(ret)
                   : "Nd"(port) );
    return ret;
}
unsigned char kbdus[128] =
{
    0,  27, '1', '2', '3', '4', '5', '6', '7', '8',	/* 9 */
  '9', '0', '-', '=', '\b',	/* Backspace */
  '\t',			/* Tab */
  'q', 'w', 'e', 'r',	/* 19 */
  't', 'y', 'u', 'i', 'o', 'p', '[', ']', '\n',	/* Enter key */
    'T',			/* 29   - Control */
  'a', 's', 'd', 'f', 'g', 'h', 'j', 'k', 'l', ';',	/* 39 */
 '\'', '`',   'S',		/* Left shift */
 '\\', 'z', 'x', 'c', 'v', 'b', 'n',			/* 49 */
  'm', ',', '.', '/',   'S',				/* Right shift */
  '*',
    'A',	/* Alt */
  ' ',	/* Space bar */
    'C',	/* Caps lock */
   	/* 59 - F1 key ... > */
    '!','@','#','$','%','^','&','*','(',
    ')',	/* < ... F10 */
    0,	/* 69 - Num lock*/
    0,	/* Scroll Lock */
    0,	/* Home key */
    'U',	/* Up Arrow */
    0,	/* Page Up */
  '-',
    'L',	/* Left Arrow */
    0,
    'R',	/* Right Arrow */
  '+',
    0,	/* 79 - End key*/
    'D',	/* Down Arrow */
    0,	/* Page Down */
    0,	/* Insert Key */
    0,	/* Delete Key */
    0,   0,   0,
    0,	/* F11 Key */
    0,	/* F12 Key */
    0,	/* All other keys are undefined */
};
char key;
int kbd_provokers = 0;
void *kbd_provokes[16] =
{
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0
};
void setkeyboard_provoke(void(*run)(char c))
{
	kbd_provokes[kbd_provokers] = run;
	kbd_provokers++;
}
void keyboard_provoke()
{
	for (int i = 0; i < kbd_provokers; i++)
	{
		void (*run)(char c) = kbd_provokes[i];
		run(key);
	}
}
void keyboard_handler(struct regs *r)
{
    unsigned char scancode;
    scancode = inb(0x60);
    key = kbdus[scancode];
	if (scancode & 0x80)
    {
		
    }
    else
    {
        keyboard_provoke();
    }
}
