#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
extern void terminal_writestring();
static inline uint8_t inb(uint16_t port)
{
    uint8_t ret;
    asm volatile ( "inb %1, %0"
                   : "=a"(ret)
                   : "Nd"(port) );
    return ret;
}
static inline void outb(uint16_t port, uint8_t val)
{
	asm volatile ( "outb %0, %1" : : "a"(val), "Nd"(port) );
}
unsigned char gdump[] =
{
	0x63, 0x03, 0x01, 0x0F, 0x00, 0x0E, 0x5F, 0x4F, 
	0x50, 0x82, 0x54, 0x80, 0xBF, 0x1F, 0x00, 0x41, 
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x9C, 0x0E, 
	0x8F, 0x28,	0x40, 0x96, 0xB9, 0xA3, 0xFF, 0x00, 
	0x00, 0x00, 0x00, 0x00, 0x40, 0x05, 0x0F, 0xFF,
	0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07,
	0x08, 0x09, 0x0A, 0x0B, 0x0C, 0x0D, 0x0E, 0x0F,
	0x41, 0x00, 0x0F, 0x00,	0x00
};
void setg(unsigned char *dump)
{
	unsigned int i;
	outb(0x3C2, *dump);
	dump++;
	for(i = 0; i < 5; i++)
	{
		outb(0x3C4, i);
		outb(0x3C5, *dump);
		dump++;
	}
	outb(0x3D4, 0x03);
	outb(0x3D5, inb(0x3D5) | 0x80);
	outb(0x3D4, 0x11);
	outb(0x3D5, inb(0x3D5) & ~0x80);
	dump[0x03] |= 0x80;
	dump[0x11] &= ~0x80;
	for(i = 0; i < 25; i++)
	{
		outb(0x3D4, i);
		outb(0x3D5, *dump);
		dump++;
	}
	for(i = 0; i < 9; i++)
	{
		outb(0x3CE, i);
		outb(0x3CF, *dump);
		dump++;
	}
	for(i = 0; i < 21; i++)
	{
		(void)inb(0x3DA);
		outb(0x3C0, i);
		outb(0x3C0, *dump);
		dump++;
	}
	(void)inb(0x3DA);
	outb(0x3C0, 0x20);
}
int buffer[321][201];
enum vga_color {
	BLACK = 0,
	BLUE = 1,
	GREEN = 2,
	CYAN = 3,
	RED = 4,
	MAGENTA = 5,
	YELLOW = 6,
	LIGHT_GREY = 7,
	DARK_BLUE = 8,
	LIGHT_BLUE = 9,
	LIGHT_GREEN = 10,
};
static unsigned char* vga_mem = (unsigned char *) 0xA0000;
void vgasetp(int x, int y, int c) {
   if (x > 320) return;
   if (y > 200) return;
   if (c > 256) return;
   vga_mem[320 * y + x] = c;
}
void setp(int x, int y, int c)
{
	buffer[x][y] = c;
}
void vga_clear(int c)
{
   for(int y = 0; y < 200; y++)
      for(int x = 0; x < 320; x++)
         setp(x, y, c);
}
void hline(int x, int y, int idk, int c)
{
	for (int a = x; a < x + idk; a++)
		setp(a, y, c);
}
void vline(int x, int y, int idk, int c)
{
	for (int a = y; a < y + idk; a++)
		setp(x, a, c);
}
void fillrectangle(int x, int y, int width, int height, int c)
{
	for (int a = x; a < x + width; a++)
		for (int b = y; b < y + height; b++)
			setp(a, b, c);
}
void rectangle(int x, int y, int width, int height, int c)
{
    hline(x, y, --width, c);
    hline(x, y + --height, width, c);
    vline(x, y, height, c);
    vline(x + width, y, ++height, c);
}
void tick()
{
	for (int x = 0; x < 320; x++)
		for (int y = 0; y < 200; y++)
			vgasetp(x, y, buffer[x][y]);
}
extern void ui_init();
void vgasetup()
{
	setg(gdump);
	vga_clear(0);
	ui_init();
}
