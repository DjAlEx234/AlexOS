#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#define PORT 0x3f8
static inline uint8_t inb(uint16_t port)
{
    uint8_t ret;
    asm volatile ( "inb %1, %0"
                   : "=a"(ret)
                   : "Nd"(port) );
    return ret;
}
static inline void outb(uint16_t port, uint8_t val)
{
    asm volatile ( "outb %0, %1" : : "a"(val), "Nd"(port) );
}
int is_transmit_empty() {
    return inb(PORT + 5) & 0x20;
}
void write_serial(char a) {
    while (is_transmit_empty() == 0);
    outb(PORT,a);
}
extern int strlen();
void serial_outs(char* string)
{
    for (int i = 0; i < strlen(string); i++)
		write_serial(string[i]);
	//write_serial('\n');
}
extern char* itoa();
void serial_outi(int num)
{
    char* buf = "";
    char* string = itoa(num, buf, 10);
    serial_outs(string);
}
void init_serial() {
   outb(PORT + 1, 0x00);
   outb(PORT + 3, 0x80);
   outb(PORT + 0, 0x03);
   outb(PORT + 1, 0x00);
   outb(PORT + 3, 0x03);
   outb(PORT + 2, 0xC7);
   outb(PORT + 4, 0x0B);
}
